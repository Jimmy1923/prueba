<?php

use Illuminate\Database\Seeder;
use App\Products;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new Products();
            $product->name = "SKU01";
            $product->sku = 100;
            $product->price = 125.50;
        $product->save();        

        $product2 = new Products();
            $product2->name = "SKU02";
            $product2->sku = 100;
            $product2->price = 210.00;
        $product2->save();        

        $product3 = new Products();
            $product3->name = "SKU03";
            $product3->sku = 100;
            $product3->price = 50.00;
        $product3->save();
    }
        
}