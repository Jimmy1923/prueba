<!DOCTYPE html>

<html lang="en">
    <!-- Inicio del Head -->
    <head>
        <meta charset="utf-8">
        <link href="{{ asset('images/logo.svg')}}" rel="shortcut icon">
        <title>Prueba | @yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Sitio de prueba.">
        <meta name="author" content="Jimmy Frias">
        <!-- Inicio del CSS de la pagina-->
        <link rel="stylesheet" href="{{ asset('css/app.css')}}" />
        <!-- Finalización del CSS de la pagina-->
    </head>
    <!-- Finalización del Head -->
    <body class="app">
        <!-- Inicio del Menu -->
        @include('includes.panel.menu')
        <!-- Finalización del Menu -->
            <!-- Incio del contenedor -->
            <div class="content">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <p>Corrige los siguientes errores:</p>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                @section('content')
                @show

            </div>
            <!-- Finalización del contenedor -->
        <!-- Incio del JS Assets-->

        <script src="{{ asset('js/app.js')}}"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        @section('scripts_ready')
        @show
        @include('sweet::alert')
        <!-- Finalización: JS Assets-->
    </body>
</html>
