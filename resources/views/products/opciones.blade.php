<div class="flex justify-center items-center">
    <a class="flex items-center text-theme-3" href="javascript:;" data-toggle="modal" data-target="#edit-products{{$id}}" value="{{$id}}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Editar </a>
    @include('products.updated')
</div>
