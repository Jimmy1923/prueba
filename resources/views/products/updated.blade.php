<div class="modal" id="edit-products{{ $id}}">
    <div class="modal__content modal__content--xl">
        <div class="flex items-center px-5 py-5 sm:py-3 border-b border-gray-200">
            <h2 class="font-medium text-base mr-auto">
                Actualizacion del producto {{$name}}
            </h2>

        </div>
        {!! Form::open(['route' => ['products.update', $id], 'method' => 'PUT']) !!}
        {{ csrf_field() }}
            <div class="p-5 grid grid-cols-12 gap-4 row-gap-3">
                <div class="col-span-12 sm:col-span-4 @if ($errors->has('name')) has-error @endif">
                    <label class="font-medium text-base mr-auto" for="name">Nombre:</label>
                    <input type="text" class="input w-full border mt-2 flex-1" name="name" id="name" value="{{ $name }}" aria-describedby="helpNombre" required>
                    <span id="helpNombre" class="help-block">
                        @if ($errors->has('name'))
                            @foreach($errors->get('name') as $message)
                                @if(!$loop->first) / @endif
                                {{ $message }}
                            @endforeach
                        @endif
                    </span>
                </div>
                <div class="col-span-12 sm:col-span-4 @if ($errors->has('sku')) has-error @endif">
                    <label class="font-medium text-base mr-auto" for="sku">Sku:</label>
                    <input type="number" class="input w-full border mt-2 flex-1" min="1" name="sku" id="sku" value="{{ $sku }}" aria-describedby="helpSku" required>
                    <span id="helpSku" class="help-block">
                        @if ($errors->has('sku'))
                            @foreach($errors->get('sku') as $message)
                                @if(!$loop->first) / @endif
                                {{ $message }}
                            @endforeach
                        @endif
                    </span>
                </div>
                <div class="col-span-12 sm:col-span-4 @if ($errors->has('price')) has-error @endif">
                    <label class="font-medium text-base mr-auto" for="price">Precio:</label>
                    <input type="number" class="input w-full border mt-2 flex-1" min="1"  name="price" id="price" value="{{ $price }}" aria-describedby="helpPrice" required>
                    <span id="helpPrice" class="help-block">
                        @if ($errors->has('price'))
                            @foreach($errors->get('price') as $message)
                                @if(!$loop->first) / @endif
                                {{ $message }}
                            @endforeach
                        @endif
                    </span>
                </div>
                <div class="col-span-12 sm:col-span-12">
                </div>
            </div>
            <div class="px-5 py-3 text-right border-t border-gray-200">
                <button type="button" data-dismiss="modal" class="button w-20 border text-gray-700 mr-1">Cancelar</button>
                <button class="button w-20 bg-theme-1 text-white">Guardar</button>
            </div>
        {!! Form::close()!!}
    </div>
</div>

