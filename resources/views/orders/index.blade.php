@extends('panel')
@section('title', 'Ordenes')
@section('content')
<style type="text/css">
 .table {
    width: 100%;
    text-align: center;
}
</style>
<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Ordenes creadas
    </h2>
    <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
        <a class="button text-white bg-theme-1 shadow-md mr-2" href="{{ route('orders.create') }}" >Crear nueva orden</a>

    </div>
</div><br>
<!-- BEGIN: Datatable -->
<div class="intro-y datatable-wrapper overflow-x-auto">
    <table class="table table-report table-report--bordered display datatable w-full">
        <thead>
            <tr>
                <th class="border-b-2 whitespace-no-wrap">Id</th>
                <th class="border-b-2 text-center whitespace-no-wrap">Productos vendidos</th>
                <th class="border-b-2 text-center whitespace-no-wrap">Total</th>
                <th class="border-b-2 text-center whitespace-no-wrap">Opciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<!-- END: Datatable -->
@endsection
@section('scripts_ready')
<script type="text/javascript">
    $(function () {
      $('#DataTables_Table_0').DataTable().destroy();
      var table = $('#DataTables_Table_0').DataTable({
          processing: true,
          serverSide: true,
        ajax: "{{ route('orders.index') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'orderItems', name: 'orderItems'},
            {data: 'total', name: 'total'},
            {data: 'btn', name: 'btn'},
        ],
            "language": {
                "info": "_TOTAL_ registros",
                "search": "Buscar",
                "paginate": {
                    "next": "Siguiente",
                    "previous": "Anterior",
                },
                "lengthMenu": 'Mostrar <select >'+
                            '<option value="10">10</option>'+
                            '<option value="30">30</option>'+
                            '<option value="-1">Todos</option>'+
                            '</select> registros',
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "emptyTable": "No hay datos",
                "zeroRecords": "No hay coincidencias",
                "infoEmpty": "",
                "infoFiltered": ""
            }
    });
  });
</script>
@endsection
