@extends('panel')
@section('title', 'Ver compra')
@section('content')
<div>
    <div class="flex items-center px-5 py-5 sm:py-3 border-b border-gray-200">
    <h2 class="font-medium text-base mr-auto">
       Orden No. {{$order->id}}
    </h2>
</div>

<div class="p-5 grid grid-cols-12 gap-4 row-gap-3">
    <div class="col-span-12 sm:col-span-12">
    </div>
    <div class="rounded-md flex items-center col-span-12 px-5 py-3 mb-2 bg-theme-5 text-gray-700"> <i data-feather="user" class="w-6 h-6 mr-2"></i> Datos de la orden </div>
     <div class="col-span-12 sm:col-span-12">
    </div>
    <div class="col-span-12 sm:col-span-12">
        <div class="overflow-x-auto">
            <table class="table" id="details">
                <thead>
                     <tr class="bg-gray-700 text-white" >
                         <th class="text-center border-b whitespace-no-wrap">id</th>
                         <th class="text-center border-b whitespace-no-wrap">Fecha de creacion</th>
                         <th class="text-center border-b whitespace-no-wrap">Producto</th>
                         <th class="text-center border-b whitespace-no-wrap">Cantidad</th>
                         <th class="text-center border-b whitespace-no-wrap">Precio</th>
                         <th class="text-center border-b whitespace-no-wrap">Subtotal</th>
                     </tr>
                 </thead>
                 <tfoot>
                    <th colspan="4"></th>
                    <th>Total Final</th>
                    <th><strong>$ {{ number_format($order->total, 2)}}</strong></th>
                </tfoot>
                 <tbody>
                    @foreach($order->orderItems as $detail)
                        <tr>
                            <td class="text-center border-b">{{ $detail->id}}</td>
                            <td class="text-center border-b">{{ $detail->created_at}}</td>
                            <td class="text-center border-b">{{ $detail->product->name}}</td>
                            <td class="text-center border-b">{{ $detail->quantity }}</td>
                            <td class="text-center border-b">{{ number_format($detail->product->price, 2)}}</td>
                            <td class="text-center border-b">{{ number_format($detail->total, 2)}}</td>
                        </tr>
                    @endforeach
                 </tbody>
             </table>
        </div>
    </div>
    <div class="px-5 py-3 text-right border-t border-gray-200">
        <a href="/" class="button w-32 mr-2 mb-2 flex items-center justify-center border text-gray-700"> <i data-feather="hard-drive" class="w-4 h-4 mr-2"></i> Regresar</a>
    </div>
@endsection
