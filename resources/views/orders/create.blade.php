@extends('panel')
@section('title', 'Crear orden')
@section('content')
<div>
    <div class="flex items-center px-5 py-5 sm:py-3 border-b border-gray-200">
    <h2 class="font-medium text-base mr-auto">
       Crear nueva orden
    </h2>
</div>
{!! Form::open(['route' => 'orders.store']) !!}
<div class="p-5 grid grid-cols-12 gap-4 row-gap-3">
    <div class="rounded-md flex items-center col-span-12 px-5 py-3 mb-2 bg-theme-5 text-gray-700"> <i data-feather="shopping-cart" class="w-6 h-6 mr-2"></i> Datos de la orden </div>
    <div class="col-span-12 sm:col-span-6 @if ($errors->has('product_id')) has-error @endif">
        <label class="font-medium text-base mr-auto">Nombre del Producto</label>
        <div class="mt-2">
             <select class="select2 w-full" id="productos_idcompras" name="product_id">
             	    <option value="">Selecciona un producto</option>
                @foreach($product as $products)
                	<option value="{{$products->id}}">{{$products->name}}</option>
                @endforeach

            </select>
        </div>
    </div>
    <div class="col-span-12 sm:col-span-6"><br><br>
    	<a class="button bg-theme-32 text-white" data-toggle="modal" data-target="#crear_productos">Agregar Productos</a>

    </div>
    <div class="col-span-12 sm:col-span-12">
    </div>
    <div class="col-span-12 sm:col-span-4 @if ($errors->has('precioproducto')) has-error @endif">
        <label class="font-medium text-base mr-auto" for="precioproducto">Precio</label>
        <input type="text" class="input w-full border mt-2 flex-1" name="precioproducto" id="price_product" min="1" value="{{old('precioproducto')}}" aria-describedby="helpPrecio" disabled>
        <span id="helpPrecio" class="help-block">
            @if ($errors->has('precio'))
                @foreach($errors->get('precio') as $message)
                    @if(!$loop->first) / @endif
                    {{ $message }}
                @endforeach
            @endif
        </span>
    </div>
    <div class="col-span-12 sm:col-span-4 @if ($errors->has('cantidad')) has-error @endif">
        <label class="font-medium text-base mr-auto" for="cantidad">Cantidad</label>
        <input type="number" class="input w-full border mt-2 flex-1" name="cantidad" id="cantidad" value="{{old('cantidad')}}" min="1" aria-describedby="helpCantidad">
        <span id="helpCantidad" class="help-block">
            @if ($errors->has('cantidad'))
                @foreach($errors->get('cantidad') as $message)
                    @if(!$loop->first) / @endif
                    {{ $message }}
                @endforeach
            @endif
        </span>
    </div>
     <div class="col-span-12 sm:col-span-4"><br><br>
    	<a class="button bg-theme-9 text-white" id="btn_add">Agregar Productos</a>
    </div>
    <div class="col-span-12 sm:col-span-12">
    </div>
    <br>
    <div class="col-span-12 sm:col-span-12">
	    <div class="overflow-x-auto">
		    <table class="table" id="details">
		        <thead>
		             <tr class="bg-gray-700 text-white" >
		                 <th class="border-b-2 whitespace-no-wrap">Opciones</th>
		                 <th class="border-b-2 whitespace-no-wrap">Producto</th>
                         <th class="border-b-2 whitespace-no-wrap">Precio</th>
		                 <th class="border-b-2 whitespace-no-wrap">Cantidad</th>
		                 <th class="border-b-2 whitespace-no-wrap">Subtotal</th>
		             </tr>
		         </thead>
		        <tfoot>
					<th colspan="3"></th>
					<th>Total</th>
					<th><h4 id="total">$. 0.00</h4><input type="hidden" name="total" id="total_re"></input></th>
				</tfoot>
		         <tbody>


		         </tbody>
		     </table>
	 	</div>
	</div>

<div class="px-5 py-3 text-right border-t border-gray-200" id="save">
    <button onclick="window.location.href='/'" type="button" class="button w-20 border text-gray-700 mr-1">Regresar</button>
    <button class="button w-20 bg-theme-1 text-white" id="submit">Crear</button>
</div>
</div>

{!! Form::close()!!}
@endsection

@section('scripts_ready')
<script type="text/javascript">
    $(function() {
        $("#productos_idcompras").on('change', onSelectproductChanges);
    });

    function onSelectproductChanges() {
		var product_id = $(this).val();
		$.get('/product/'+product_id, function (data){
			$('#price_product').val(data.price);
		});
	}

    $(document).ready(function(){
		$("#btn_add").click(function(){
			agregar();
		});
	});

    var cont = 0;
	total = 0;
	subtotal=[];
	$("#save").hide();
	$("#proveedor_id").change(showValues);

	function showValues() {
		dataProduct = document.getElementById('productos_idcompras').value.split('_');
		$("#precioproducto").val(dataProduct[1]);
	}

	function agregar() {
		dataProduct = document.getElementById('productos_idcompras').value.split('_');
		$("#precioproducto").val(dataProduct[1]);

		productos_idcompras = dataProduct[0];
		product = $("#productos_idcompras option:selected").text();
		quantity = $("#cantidad").val();
		price = $("#price_product").val();

		if (productos_idcompras!="" && quantity!="" && quantity > 0  && price!="")
		{
			subtotal[cont] = (quantity*price);
			total=total + subtotal[cont];
			var fila = '<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-raised btn-warning btn-xs" onclick="eliminar('+cont+');">x</button></td><td><input type="hidden" name="productos_idcompras[]" value="'+productos_idcompras+'">'+product+'</td><td><input type="hidden" name="price[]" class="field" value="'+price+'">'+price+'</td><td><input type="hidden" name="quantity[]" class="field" value="'+quantity+'">'+quantity+'</td><td class="field">'+subtotal[cont]+'</td></tr>';
			cont++;
			evaluate();
			clean();
			$('#total').html("$ " + total);
			$('#total_re').val(total);
			$('#details').append(fila);
		}else {
			alert('Error al ingresar el detalle de la requisición, revise los datos del producto');
		}

	}

	function clean() {
		$("#cantidad").val("");
		$("#price_product").val("");
	}

	function evaluate() {
		if (total>0) {
			$("#save").show();

		}else {
			$("#save").hide();
		}
	}

	function eliminar(index) {
		total = total-subtotal[index];
		$("#total").html("$ " +total);
		$("#total_sale").val(total);
		$("#fila" + index).remove();
		evaluate();
	}
</script>

@endsection
