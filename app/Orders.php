<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{

    protected $table = 'orders';

    protected $fillable = [
        'total'
    ];

    public function orderItems(){
        return $this->hasMany(Order_items::class, 'order_id');
    }
}
