<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_items extends Model
{
    protected $table = 'order_items';

    protected $fillable = [
        'order_id','product_id','quantity','total'
    ];

    public function order(){
        return $this->belongsTo(Orders::class, 'order_id');
    }

    public function product(){
        return $this->belongsTo(Products::class, 'product_id');
    }
}
