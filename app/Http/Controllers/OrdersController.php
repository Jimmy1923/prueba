<?php

namespace App\Http\Controllers;

use App\Orders;
use App\Products;
use App\Order_items;
use Illuminate\Http\Request;
use DB;
use DataTables;
use OrderItems;

class OrdersController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Orders::all();
             return Datatables::of($data)
                    ->editColumn('orderItems', function($datas){
                        return $datas->orderItems->count();
                     })
                    ->addIndexColumn()
                    ->addColumn('btn', 'orders.opciones')
                    ->rawColumns(['btn'])
                    ->toJson();
         }
        return view('orders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = Products::all();
        return view('orders.create',compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $order = new Orders($request->all());
                $order->total = $request->get('total');
            $order->save();

            $idproduct = $request->get('productos_idcompras');
            $quantity = $request->get('quantity');
            $price = $request->get('price');
            $cont = 0;

            while ($cont < count($idproduct)) {
                $details = new Order_items();
                $details->order_id = $order->id;
                $details->product_id  = $idproduct[$cont];
                $details->quantity = $quantity[$cont];
                $details->total = $price[$cont]*$quantity[$cont];

                $details->save();
                $cont = $cont+1;
            }
            DB::commit();
        alert()->success('La compra ha sido realizada.', 'Compra agregada');
        } catch (Exception $e) {
            DB::rollback();
        }
        return redirect()->route('orders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Orders::find($id);
        return view('orders.mostrar', compact('order'));
    }

    public function getProduct($id)
    {
        if (request()->ajax()) {
            return Products::findOrFail($id);
        }
        return back();
    }
}
