<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;
use DataTables;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Products::all();
             return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('btn', 'products.opciones')
                    ->rawColumns(['btn'])
                    ->toJson();
         }
        return view('products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'sku' => 'required',
            'price' => 'required'
            ]
        );
    Products::create($request->all());

    alert()->success('El producto ha sido creado correctamente.', 'Producto creado');
    return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show(Products $products)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $products)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Products $products, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'sku' => 'required',
            'price' => 'required'
            ]
        );
        $product = Products::find($id)->update($request->except(['_method', '_token']));
        alert()->info('El producto ha sido actualizado correctamente.', 'Producto actualizado');
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $products)
    {
        //
    }
}
