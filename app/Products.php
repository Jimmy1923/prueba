<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'name','sku','price'
    ];

    public function orderItem(){
        return $this->hasMany('App\Models\Order_items');
    }
}
